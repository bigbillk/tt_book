#pragma once

//Includs
#include <iostream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <time.h>
//Dice Types
#define D4 4
#define D6 6
#define D8 8
#define D10 10
#define D12 12
#define D20 20

//Stat Types
#define BLOCK_HP	":HP:"
#define BLOCK_AC	":AC:"
#define BLOCK_MOV	":MOV:"
#define BLOCK_STR	":STR:"
#define BLOCK_DEX	":DEX:"
#define BLOCK_WIS	":WIS:"
#define BLOCK_INT	":INT:"
#define BLOCK_CRI	":CRI:"
#define BLOCK_CON	":CON:"

//Block Types
#define TITLE			":T:"

#define BEGIN_DESCRIPTION	":D:"
#define END_DESCRIPTION		":d:"

#define BEGIN_BLOCK	":B:"
#define END_BLOCK	":b:"

//Block Inserts
#define SPLIT	":SPLIT:"


//Macros


namespace TT_Book {

	enum DamageType {
		Percing,
		Blugening,
		Slashing
	};

	enum PageType {
		Type_List,
		Type_Block
	};
	struct Block {
		const char* Title;
		const char* Desctipyion;
	};

	struct List {
		const char* ListName;
		
	};

	class Page {
	private:
		PageType m_PageType;
		std::vector<Block> m_PageBlocks;
		std::vector<List> m_PageLists;

		void SetBlocks();
		void SetList();
		void SetListType(PageType pageType) {m_PageType = pageType;}

	public:
		const char* pageTitle;
		
		unsigned int GetBlockAmount()		{return m_PageBlocks.size();}
		PageType GetPageType()				{return m_PageType;}
		Block GetBlock(unsigned int pos) 	{return m_PageBlocks[pos];}
		List GetList(unsigned int pos)		{return m_PageLists[pos];}
		

	};
	struct CreacherBlock : public Block {
		int STR;
		int DEX;
		int WIS;
		int CON;
		int CRI;
		int INT;

		int AC;
		int HP;

	};

	struct ItemBlock : public Block {
		std::string Name() {return Title;};
		void SetEffect(const char* effect) {m_Effect = effect;}

	private:
		std::string m_Effect;
	};

	struct WeaponBlock : public ItemBlock {

		DamageType damageType;
		std::string WeaponDamage;
		std::string WeaponEffects;

	};

	//String exstracter
	class Exstracter {
	public:

		const char* StringExstracter(const char* filePath);

		const char** FormatExstracter(const char* page, unsigned int pageSize);
	private:
		std::vector<std::string> pageBlocks;
	};


	//Format exstracter

	//Print Stats
	//Print Title
	//Print Block
	//Format Inserts

	//Math Commands
	class Command {
	public:
		static int Calc_Stat(int stat);
		static int Role(int dt) {srand(time(NULL)); return 1;}
	};
}
